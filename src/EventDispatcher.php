<?php 
namespace DarioRieke\EventDispatcher;

use DarioRieke\EventDispatcher\EventDispatcherInterface;
use Psr\EventDispatcher\StoppableEventInterface;
use Psr\EventDispatcher\ListenerProviderInterface;

/**
 * EventDispatcher
 */
class EventDispatcher implements EventDispatcherInterface {
	/**
	 * array of listeners
	 * @var callable[]
	 */
	private $listeners = [];

	/**
	 * array of event listenerProviders
	 * @var EventSubscriberInterface[]
	 */
	private $listenerProviders = [];


	public function addListener(string $eventName, callable $callback, int $priority = null) {
		if($priority === null) {
			$this->listeners[$eventName][] = $callback;
		}
		else {
			$this->listeners[$eventName][$priority] = $callback;	
		}
	}


	public function addListenerProvider(ListenerProviderInterface $provider) {
		$this->listenerProviders[] = $provider;
	}

	public function getListenerProviders(): iterable {
		return $this->listenerProviders;
	}


	public function dispatch(object $event, string $eventName = null): object {
		$eventName = $eventName ?? get_class($event);

		$stoppable = $event instanceof StoppableEventInterface; 

		if($this->hasListeners($eventName)) {
			foreach ($this->getListenersForEvent($eventName) as $listener) {
				//if it is a stoppable event, check if propagation is stopped 
				if($stoppable && $event->isPropagationStopped()) break;

				$listener($event);
			}
		}

		foreach ($this->listenerProviders as $listenerProvider) {
			$listeners = $listenerProvider->getListenersForEvent($event);

			foreach ($listeners as $listener) {
				//if it is a stoppable event, check if propagation is stopped 
				if($stoppable && $event->isPropagationStopped()) break 2;

				$listener($event);
			}
		}

		return $event;
	}

	public function getListenersForEvent(string $eventName): iterable {
		//sort listeners by priority
		ksort($this->listeners[$eventName]);
		return $this->listeners[$eventName] ?? [];
	}

	/**
	 * check if event has listeners
	 * @param string   $eventName  name of event
	 * @return boolean 
	 */
	private function hasListeners(string $eventName): bool {
		return isset($this->listeners[$eventName]);
	}
} 
 ?>