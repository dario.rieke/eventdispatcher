# EventDispatcher

A PSR-14 implementation providing an EventDispatcher.

## Installation

install via composer
```json
    "require": {
        "dariorieke/event-dispatcher": "dev-master"
    }
```

## Running tests
run tests with the following command
```
./vendor/bin/phpunit .\tests
```

## Usage

### Register a listener
Listeners registered via `addListener` get called before registered ListenerProviders.
Listeners can be attached to event names.  
```php
use DarioRieke\EventDispatcher\EventDispatcher;

$dispatcher = new EventDispatcher(); 
$dispatcher->addListener('myEvent', function($event) {
	//do stuff here...
});
```

### Register a ListenerProvider
ListenerProviders get called in the same order they are attached to the EventDispatcher, but always after listeners registered via `addListener`.
Take a look at the PSR-14 `ListenerProviderInterface` for further information.
```php
$dispatcher->addListenerProvider($psr14ListenerProvider);
```

### Dispatch an Event
You can dispatch any object as an event. If you pass an instance of `StoppableEventInterface` the events propagation to other listeners can be stopped. See PSR-14 `StoppableEventInterface` for further information.
```php
$dispatcher->dispatch($myEventObject);
```

If no event name is provided when dispatching an event, the classname is used. Instances of the same class get treated as the same event, except you explicitly set an event name when dispatching:
```php
$dispatcher->dispatch($myEventObject, 'customEventName');
```